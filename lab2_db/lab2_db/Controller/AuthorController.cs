﻿using lab2_db.Database;
using lab2_db.Model;
using lab2_db.View;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace lab2_db.Controller
{
    class AuthorController : EntityController
    {
        AuthorDAO dao;
        public AuthorController(DBConnection db, MyView view) : base(db, view)
        {
            dao = new AuthorDAO(db);
        }

        public override void start()
        {

            char key = ' ';
            while (key != 'b')
            {
                view.printSubMenu("Author");
                view.waitForUnpressKey();
                key = view.getKey();
                switch (key)
                {
                    case '1':
                        Add();
                        break;
                    case '2':
                        GetById();
                        break;
                    case '3':
                        GetAllFromPage();
                        break;
                    case '4':
                        Delete();
                        break;
                    case '5':
                        Update();
                        break;
                    case 'b':
                        break;
                    default:
                        view.printWrongInput();
                        break;
                }
            }
        }

        public override void Add()
        {
            view.clearScreen();
            String name = getStringFromMess("Enter author name",20);
            String bio = getStringFromMess("Enter author bio",100);
            dao.Create(new Author(-1, name,bio));
        }

        public override void Delete()
        {
            view.clearScreen();
            int id = getNaturalNumberFromMess("Enter id author to delete");
            dao.Delete(id);
        }

        public override void GetAllFromPage()
        {
            view.clearScreen();
            int page = getNaturalNumberFromMess("Enter page authors to get list");
            List<Author> list = dao.Get(page);
            view.authorListToString(list, page);
            view.wait();
        }

        public override void GetById()
        {
            view.clearScreen();
            int id = getNaturalNumberFromMess("Enter id author to get");
            Author author = dao.Get((long)id);
            view.authorToString(author);
            view.wait();
        }


        public override void Update()
        {
            view.clearScreen();
            int id = getNaturalNumberFromMess("Enter id author to update");
            Author author = dao.Get((long)id);
            String name = getStringFromMess("Enter author name(Prew was " + author.Name + ")",20);
            String bio = getStringFromMess("Enter author bio(Prew was " + author.Bio + ")",100);
            author.Name = name;
            author.Bio = bio;
            dao.Update(author);
        }
    }
}
