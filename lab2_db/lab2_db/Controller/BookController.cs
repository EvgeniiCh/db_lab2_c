﻿using lab2_db.Database;
using lab2_db.Model;
using lab2_db.View;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace lab2_db.Controller
{
    class BookController : EntityController
    {
        BookDAO dao;
        public BookController(DBConnection db, MyView view) : base(db, view)
        {
            dao = new BookDAO(db);
        }
        public override void start()
        {

            char key = ' ';
            while (key != 'b')
            {
                view.printSubMenu("Book");
                view.waitForUnpressKey();
                key = view.getKey();
                switch (key)
                {
                    case '1':
                        Add();
                        break;
                    case '2':
                        GetById();
                        break;
                    case '3':
                        GetAllFromPage();
                        break;
                    case '4':
                        Delete();
                        break;
                    case '5':
                        Update();
                        break;
                    case 'b':
                        break;
                    default:
                        view.printWrongInput();
                        break;
                }
            }
        }

        public override void Add()
        {
            view.clearScreen();
            String name = getStringFromMess("Enter book name", 200);
            String text = getStringFromMess("Enter book text", 10000);
            int reader_id = getNaturalNumberFromMess("Enter id reader for book");
            int author_id = getNaturalNumberFromMess("Enter id author for book");
            dao.Create(new Book(-1, name, text, author_id, reader_id));
        }

        public override void Delete()
        {
            view.clearScreen();
            int id = getNaturalNumberFromMess("Enter id book to delete");
            dao.Delete(id);
        }

        public override void GetAllFromPage()
        {
            view.clearScreen();
            int page = getNaturalNumberFromMess("Enter page books to get list");
            List<Book> list = dao.Get(page);
            view.booksListToString(list, page);
            view.wait();
        }

        public override void GetById()
        {
            view.clearScreen();
            int id = getNaturalNumberFromMess("Enter id reader to get");
            Book book = dao.Get((long)id);
            view.bookToString(book);
            view.wait();
        }

        public override void Update()
        {
            view.clearScreen();
            int id = getNaturalNumberFromMess("Enter id book to update");
            Book book = dao.Get((long)id);
            String name = getStringFromMess("Enter book name(Prew was " + book.Name + ")", 200);
            String text = getStringFromMess("Enter book text(Prew was " + book.Text + ")", 10000);
            int reader_id = getNaturalNumberFromMess("Enter book reader id(Prew was " + book.Reader_id + ")");
            book.Name = name;
            book.Text =text;
            book.Reader_id = reader_id;
            dao.Update(book);
        }
    }
}
