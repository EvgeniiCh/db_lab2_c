﻿using lab2_db.Database;
using lab2_db.View;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace lab2_db.Controller
{
    abstract class EntityController
    {
        protected DBConnection db;
        protected MyView view;

        public EntityController(DBConnection db,MyView view)
        {
            this.db = db;
            this.view = view;
        }
        public abstract void start();
        public abstract void Add();
        public abstract void GetById();
        public abstract void GetAllFromPage();
        public abstract void Update();
        public abstract void Delete();
        

        protected int getNaturalNumberFromMess(String mess)
        {
            int id = -1;
            while (true)
            {
                view.printMess(mess);
                id = Convert.ToInt32(view.getString());
                if(id >= 0) 
                    return id;
                view.printWrongInput();
            }
            
        }
        protected String getStringFromMess(String mess,int length)
        {
            while (true)
            {
                view.printMess(mess);
                String str = view.getString();
                if (str.Length < length)
                    return str;
                view.printWrongInput();
                view.printMess("To long allows "+ length +" characters");
            }
        }

    }
}
