﻿using lab2_db.Database;
using lab2_db.View;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace lab2_db.Controller
{
    class MyController
    {
        private MyView view;
        private FullTextSearch fullTextSearch;
        private AuthorController authorController;
        private BookController bookController;
        private ReaderController readerController;
        private SubscriptionController subscriptionController;

        public MyController(DBConnection db)
        {
            this.view = new MyView();
            fullTextSearch = new FullTextSearch(db);
            authorController = new AuthorController(db,view);
            bookController = new BookController(db, view);
            readerController = new ReaderController(db, view);
            subscriptionController = new SubscriptionController(db, view);
        }
        public void Start()
        {
            char key =' ';
            while (key !='q')
            {
                view.printMenu();
                key =view.getKey();
                switch (key)
                {
                    case '1':
                        authorController.start();
                        break;
                    case '2':
                        bookController.start();
                        break;
                    case '3':
                        readerController.start();
                        break;
                    case '4':
                        subscriptionController.start();
                        break;
                    case '5':
                        fullTextSearchGetFullPhrase();
                        break;
                    case '6':
                        fullTextSearchGetAllWithNotIncludedWord();
                        break;
                    case 'q':
                        break;
                    default:
                        view.printWrongInput();
                        break;
                }
            }
            

            }
        private void fullTextSearchGetFullPhrase()
        {
            view.clearScreen();
            view.printMess("Enter phase");
            String phase = view.getString();
            List<SearchRes> list =fullTextSearch.getFullPhrase("bio", "public.authors", phase);
            view.serchResListToString(list);
            view.wait();
        }
        private void fullTextSearchGetAllWithNotIncludedWord()
        {
            view.clearScreen();
            view.printMess("Enter word");
            String phase = view.getString();
            List<SearchRes> list = fullTextSearch.getAllWithNotIncludedWord("bio", "public.authors", phase);
            view.serchResListToString(list);
            view.wait();
        }

        
            
        
    }
}
