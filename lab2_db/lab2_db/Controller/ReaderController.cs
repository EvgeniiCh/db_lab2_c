﻿using lab2_db.Database;
using lab2_db.Model;
using lab2_db.View;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace lab2_db.Controller
{
    class ReaderController : EntityController
    {
        ReaderDAO dao;
        public ReaderController(DBConnection db, MyView view) : base(db, view)
        {
            dao = new ReaderDAO(db);
        }

        public override void start()
        {

            char key = ' ';
            while (key != 'b')
            {
                view.printSubMenu("Reader");
                view.waitForUnpressKey();
                key = view.getKey();
                switch (key)
                {
                    case '1':
                        Add();
                        break;
                    case '2':
                        GetById();
                        break;
                    case '3':
                        GetAllFromPage();
                        break;
                    case '4':
                        Delete();
                        break;
                    case '5':
                        Update();
                        break;
                    case 'b':
                        break;
                    default:
                        view.printWrongInput();
                        break;
                }
            }
        }

        public override void Add()
        {
            view.clearScreen();
            String name = getStringFromMess("Enter reader name", 20);
            String bio = getStringFromMess("Enter reader bio", 100);
            int subscription_id = getNaturalNumberFromMess("Enter id subscription for reader");
            dao.Create(new Reader(-1, name, bio,subscription_id));
        }

        public override void Delete()
        {
            view.clearScreen();
            int id = getNaturalNumberFromMess("Enter id reader to delete");
            dao.Delete(id);
        }

        public override void GetAllFromPage()
        {
            view.clearScreen();
            int page = getNaturalNumberFromMess("Enter page readers to get list");
            List<Reader> list = dao.Get(page);
            view.readersListToString(list, page);
            view.wait();
        }

        public override void GetById()
        {
            view.clearScreen();
            int id = getNaturalNumberFromMess("Enter id reader to get");
            Reader reader = dao.Get((long)id);
            view.readerToString(reader);
            view.wait();
        }

        public override void Update()
        {
            view.clearScreen();
            int id = getNaturalNumberFromMess("Enter id reader to update");
            Reader reader = dao.Get((long)id);
            String name = getStringFromMess("Enter reader name(Prew was " + reader.Name + ")", 20);
            String bio = getStringFromMess("Enter reader bio(Prew was " + reader.Bio + ")", 100);
            reader.Name = name;
            reader.Bio = bio;
            dao.Update(reader);
        }
    }
}
