﻿using lab2_db.Database;
using lab2_db.Model;
using lab2_db.View;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace lab2_db.Controller
{
    class SubscriptionController : EntityController
    {
        SubscriptionDAO dao;
        public SubscriptionController(DBConnection db, MyView view) : base(db, view)
        {
            dao = new SubscriptionDAO(db);
        }
        public override void start()
        {
            
            char key = ' ';
            while (key != 'b')
            {
                view.printSubscriptionMenu();
                view.waitForUnpressKey();
                key = view.getKey();
                switch (key)
                {
                    case '1':
                        Add();
                        break;
                    case '2':
                        GetById();
                        break;
                    case '3':
                        GetAllFromPage();
                        break;
                    case '4':
                        Delete();
                        break;
                    case '5':
                        Update();
                        break;
                    case '6':
                        GetRange();
                        break;
                    case 'b':
                        break;
                    default:
                        view.printWrongInput();
                        break;
                }
            }
        }

        public override void Add()
        {
            view.clearScreen();
            int duration = getNaturalNumberFromMess("Enter duration subscription in days");
            dao.Create(new Model.Subscription(-1,duration));
        }

        public override void Delete()
        {
            view.clearScreen();
            int id = getNaturalNumberFromMess("Enter id subscription to delete");
            dao.Delete(id);
        }

        public override void GetAllFromPage()
        {
            view.clearScreen();
            int page = getNaturalNumberFromMess("Enter page subscriptions to get list");
            List<Subscription> list = dao.Get(page);
            view.subscriptionListToString(list,page);
            view.wait();
        }

        public override void GetById()
        {
            view.clearScreen();
            int id = getNaturalNumberFromMess("Enter id subscription to get");
            Subscription sub = dao.Get((long)id);
            view.subscriptionToString(sub);
            view.wait();
        }

        

        public override void Update()
        {
            view.clearScreen();
            int id = getNaturalNumberFromMess("Enter id subscription to update");
            Subscription sub = dao.Get((long)id);
            int duration = getNaturalNumberFromMess("Enter duration subscription in days(Prew duration was "+sub.Duration+")");
            sub.Duration = duration;
            dao.Update(sub);
            

        }

        public void GetRange()
        {
            view.clearScreen();
            int from = getNaturalNumberFromMess("Search subscriptions with number days from");
            int to=from;
            to = getNaturalNumberFromMess("Search subscriptions with number days from to");
            while (to <= from)
            {
                view.printMess("From >= to");
                view.printWrongInput();
                to = getNaturalNumberFromMess("Search subscriptions with number days from to");
                
            }
                
            List<Subscription> list = dao.GetRange(from,to);
            view.subscriptionListToString(list, 0);
            view.wait();
        }

    }
}
