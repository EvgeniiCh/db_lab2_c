﻿using lab2_db.Model;
using Npgsql;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace lab2_db.Database
{
    class AuthorDAO : DAO<Author>
    {
        public AuthorDAO(DBConnection db) : base(db) { }

        public override void Create(Author entity)
        {
            NpgsqlConnection connection = dbconnection.Open();
            NpgsqlCommand command = connection.CreateCommand();
            command.CommandText = "INSERT INTO public.authors (name, bio) VALUES (:name, :bio)";
            command.Parameters.Add(new NpgsqlParameter("name", entity.Name));
            command.Parameters.Add(new NpgsqlParameter("bio", entity.Bio));
            try
            {
                NpgsqlDataReader reader = command.ExecuteReader();
            }
            catch (PostgresException)
            {
                throw new Exception("Unable to create new author");
            }
            dbconnection.Close();
        }

        public override void Delete(long id)
        {
            NpgsqlConnection connection = dbconnection.Open();
            NpgsqlCommand command = connection.CreateCommand();
            command.CommandText = "DELETE FROM public.authors WHERE id = :id";
            command.Parameters.Add(new NpgsqlParameter("id", id));
            command.ExecuteNonQuery();
            dbconnection.Close();
        }

        public override Author Get(long id)
        {
            NpgsqlConnection connection = dbconnection.Open();
            NpgsqlCommand command = connection.CreateCommand();
            command.CommandText = "SELECT * FROM public.authors a WHERE a.id = :author_id";
            command.Parameters.Add(new NpgsqlParameter("author_id", id));
            NpgsqlDataReader reader = command.ExecuteReader();
            Author a = null;
            while (reader.Read())
            {
                a = new Author(Convert.ToInt32(reader.GetValue(0)), reader.GetValue(1).ToString(),
                                    reader.GetValue(2).ToString());
            }
            dbconnection.Close();
            return a;
        }

        public override List<Author> Get(int page)
        {
            List<Author> a_list = new List<Author>();
            NpgsqlConnection connection = dbconnection.Open();
            NpgsqlCommand command = connection.CreateCommand();
            command.CommandText = "SELECT * FROM public.authors" +
                " LIMIT 10 OFFSET :offset";
            command.Parameters.Add(new NpgsqlParameter("offset", (page-1) * 10));
            NpgsqlDataReader reader = command.ExecuteReader();
            while (reader.Read())
            {
                Author a = new Author(Convert.ToInt32(reader.GetValue(0)), reader.GetValue(1).ToString(),
                                    reader.GetValue(2).ToString());
                a_list.Add(a);
            }
            dbconnection.Close();
            return a_list;
        }

        public override void Update(Author entity)
        {
            NpgsqlConnection connection = dbconnection.Open();
            NpgsqlCommand command = connection.CreateCommand();
            command.CommandText = "UPDATE public.authors SET name = :name, bio = :bio WHERE id = :id";
            command.Parameters.Add(new NpgsqlParameter("id", entity.Id));
            command.Parameters.Add(new NpgsqlParameter("name", entity.Name));
            command.Parameters.Add(new NpgsqlParameter("bio", entity.Bio));
            try
            {
                NpgsqlDataReader reader = command.ExecuteReader();
            }
            catch (PostgresException)
            {
                throw new Exception("Unable to edit author");
            }
            dbconnection.Close();
        }
    }
}
