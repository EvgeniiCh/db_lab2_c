﻿using lab2_db.Model;
using Npgsql;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace lab2_db.Database
{
    class BookDAO : DAO<Book>
    {
        public BookDAO(DBConnection db) : base(db) { }

        public override void Create(Book entity)
        {
            NpgsqlConnection connection = dbconnection.Open();
            NpgsqlCommand command = connection.CreateCommand();
            command.CommandText = "INSERT INTO public.books (name, text, author_id, reader_id) VALUES (:name, :text, :author_id, :reader_id)";
            command.Parameters.Add(new NpgsqlParameter("name", entity.Name));
            command.Parameters.Add(new NpgsqlParameter("text", entity.Text));
            command.Parameters.Add(new NpgsqlParameter("author_id", entity.Author_id));
            command.Parameters.Add(new NpgsqlParameter("reader_id", entity.Reader_id));
            try
            {
                NpgsqlDataReader reader = command.ExecuteReader();
            }
            catch (PostgresException)
            {
                throw new Exception("Unable to create new book");
            }
            dbconnection.Close();
        }

        public override void Delete(long id)
        {
            NpgsqlConnection connection = dbconnection.Open();
            NpgsqlCommand command = connection.CreateCommand();
            command.CommandText = "DELETE FROM public.books WHERE id = :id";
            command.Parameters.Add(new NpgsqlParameter("id", id));
            command.ExecuteNonQuery();
            dbconnection.Close();
        }

        public override Book Get(long id)
        {
            NpgsqlConnection connection = dbconnection.Open();
            NpgsqlCommand command = connection.CreateCommand();
            command.CommandText = "select * from public.books b inner join public.authors a on b.author_id = a.id " +
                                   "inner join public.readers r on b.reader_id = r.id  inner join public.subscriptions s on r.subscription_id = s.id WHERE b.id = :book_id";
            command.Parameters.Add(new NpgsqlParameter("book_id", id));
            NpgsqlDataReader reader = command.ExecuteReader();
            Book b = null;
            while (reader.Read())
            {
                b = new Book(Convert.ToInt32(reader.GetValue(0)), reader.GetValue(1).ToString(),
                                        reader.GetValue(2).ToString(), Convert.ToInt32(reader.GetValue(3)),
                                        Convert.ToInt32(reader.GetValue(4)),
                                        reader.GetValue(6).ToString(), reader.GetValue(7).ToString(), reader.GetValue(9).ToString(),
                                        reader.GetValue(10).ToString(), Convert.ToInt32(reader.GetValue(11)), Convert.ToInt32(reader.GetValue(13)),
                                        Convert.ToDateTime(reader.GetValue(14)));
            }
            dbconnection.Close();
            return b;
        }

        public override List<Book> Get(int page)
        {
            List<Book> b_list = new List<Book>();
            NpgsqlConnection connection = dbconnection.Open();
            NpgsqlCommand command = connection.CreateCommand();
            command.CommandText = "select * from public.books b inner join public.authors a on b.author_id = a.id " +
                                   "inner join public.readers r on b.reader_id = r.id  inner join public.subscriptions s on r.subscription_id = s.id" +
                " LIMIT 10 OFFSET :offset";
            command.Parameters.Add(new NpgsqlParameter("offset", (page - 1) * 10));
            NpgsqlDataReader reader = command.ExecuteReader();
            while (reader.Read())
            {
                Book b = new Book(Convert.ToInt32(reader.GetValue(0)), reader.GetValue(1).ToString(),
                                        reader.GetValue(2).ToString(), Convert.ToInt32(reader.GetValue(3)),
                                        Convert.ToInt32(reader.GetValue(4)),
                                        reader.GetValue(6).ToString(), reader.GetValue(7).ToString(), reader.GetValue(9).ToString(),
                                        reader.GetValue(10).ToString(), Convert.ToInt32(reader.GetValue(11)), Convert.ToInt32(reader.GetValue(13)),
                                        Convert.ToDateTime(reader.GetValue(14)));

                b_list.Add(b);
            }
            dbconnection.Close();
            return b_list;
        }

        public override void Update(Book entity)
        {
            NpgsqlConnection connection = dbconnection.Open();
            NpgsqlCommand command = connection.CreateCommand();
            command.CommandText = "UPDATE public.books SET name = :name, text = :text, author_id= :author_id, reader_id= :reader_id WHERE id = :id";
            command.Parameters.Add(new NpgsqlParameter("id", entity.Id));
            command.Parameters.Add(new NpgsqlParameter("name", entity.Name));
            command.Parameters.Add(new NpgsqlParameter("text", entity.Text));
            command.Parameters.Add(new NpgsqlParameter("author_id", entity.Author_id));
            command.Parameters.Add(new NpgsqlParameter("reader_id", entity.Reader_id));
            try
            {
                NpgsqlDataReader reader = command.ExecuteReader();
            }
            catch (PostgresException)
            {
                throw;
            }
            dbconnection.Close();
        }
    }
}
