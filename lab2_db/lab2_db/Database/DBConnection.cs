﻿using lab2_db.Model;
using Npgsql;
using System;
using System.Collections.Generic;
using System.Text;

namespace lab2_db.Database
{
    class DBConnection
    {
        private NpgsqlConnection connection
            = new NpgsqlConnection("Server=127.0.0.1; Port=5432; User Id=lab2; Password=lab2; Database=lab2_db;");

        public NpgsqlConnection Open()
        {
            connection.Open();
            return connection;
        }

        public void Close()
        {
            connection.Close();
        }

       
    }
}
