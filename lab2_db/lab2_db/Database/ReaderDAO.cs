﻿using lab2_db.Model;
using Npgsql;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace lab2_db.Database
{
    class ReaderDAO : DAO<Reader>
    {
        public ReaderDAO(DBConnection db) : base(db) { }

        public override void Create(Reader entity)
        {
            NpgsqlConnection connection = dbconnection.Open();
            NpgsqlCommand command = connection.CreateCommand();
            command.CommandText = "INSERT INTO public.readers (name, bio, subscription_id) VALUES (:name, :bio, :subscription_id)";
            command.Parameters.Add(new NpgsqlParameter("name", entity.Name));
            command.Parameters.Add(new NpgsqlParameter("bio", entity.Bio));
            command.Parameters.Add(new NpgsqlParameter("subscription_id", entity.Subscription_id));
            try
            {
                NpgsqlDataReader reader = command.ExecuteReader();
            }
            catch (PostgresException)
            {
                throw new Exception("Unable to create new reader");
            }
            dbconnection.Close();
        }

        public override void Delete(long id)
        {
            NpgsqlConnection connection = dbconnection.Open();
            NpgsqlCommand command = connection.CreateCommand();
            command.CommandText = "DELETE FROM public.readers WHERE id = :id";
            command.Parameters.Add(new NpgsqlParameter("id", id));
            command.ExecuteNonQuery();
            dbconnection.Close();
        }

        public override Reader Get(long id)
        {
            NpgsqlConnection connection = dbconnection.Open();
            NpgsqlCommand command = connection.CreateCommand();
            command.CommandText = "SELECT * FROM public.readers r INNER JOIN public.subscriptions s ON r.subscription_id = s.id WHERE rm.id = :reader_id";
            command.Parameters.Add(new NpgsqlParameter("reader_id", id));
            NpgsqlDataReader reader = command.ExecuteReader();
            Reader r = null;
            while (reader.Read())
            {
                r = new Reader(Convert.ToInt32(reader.GetValue(0)), reader.GetValue(1).ToString(),
                                  reader.GetValue(2).ToString(), Convert.ToInt32(reader.GetValue(3)),
                                  Convert.ToInt32(reader.GetValue(5)), Convert.ToDateTime(reader.GetValue(6)) );
            }
            dbconnection.Close();
            return r;
        }

        public override List<Reader> Get(int page)
        {
            List<Reader> r_list = new List<Reader>();
            NpgsqlConnection connection = dbconnection.Open();
            NpgsqlCommand command = connection.CreateCommand();
            command.CommandText = "SELECT * FROM public.readers r INNER JOIN public.subscriptions s ON r.subscription_id = s.id" +
                " LIMIT 10 OFFSET :offset";
            command.Parameters.Add(new NpgsqlParameter("offset", (page-1) * 10));
            NpgsqlDataReader reader = command.ExecuteReader();
            while (reader.Read())
            {
                Reader r = new Reader(Convert.ToInt32(reader.GetValue(0)), reader.GetValue(1).ToString(),
                                  reader.GetValue(2).ToString(), Convert.ToInt32(reader.GetValue(3)),
                                  Convert.ToInt32(reader.GetValue(5)), Convert.ToDateTime(reader.GetValue(6)));
                r_list.Add(r);
            }
            dbconnection.Close();
            return r_list;
        }

        public override void Update(Reader entity)
        {
            NpgsqlConnection connection = dbconnection.Open();
            NpgsqlCommand command = connection.CreateCommand();
            command.CommandText = "UPDATE public.readers SET name = :name, bio = :bio, subscription_id = :subscription_id WHERE id = :id";
            command.Parameters.Add(new NpgsqlParameter("id", entity.Id));
            command.Parameters.Add(new NpgsqlParameter("name", entity.Name));
            command.Parameters.Add(new NpgsqlParameter("bio", entity.Bio));
            command.Parameters.Add(new NpgsqlParameter("subscription_id", entity.Subscription_id));
            try
            {
                NpgsqlDataReader reader = command.ExecuteReader();
            }
            catch (PostgresException)
            {
                throw new Exception("Unable to edit reader");
            }
            dbconnection.Close();
        }
    }
}
