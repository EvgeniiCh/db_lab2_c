﻿using lab2_db.Model;
using Npgsql;
using NpgsqlTypes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace lab2_db.Database
{
    class SubscriptionDAO : DAO<Subscription>
    {
        public SubscriptionDAO(DBConnection db) : base(db) { }

        public override void Create(Subscription entity)
        {
            NpgsqlConnection connection = dbconnection.Open();
            NpgsqlCommand command = connection.CreateCommand();
            command.CommandText = "INSERT INTO public.subscriptions (duration, date_when_get) VALUES (:duration, :date_when_get)";
            command.Parameters.Add(new NpgsqlParameter("duration", entity.Duration));
            command.Parameters.Add(new NpgsqlParameter("date_when_get", entity.Date_when_get));
            try
            {
                NpgsqlDataReader reader = command.ExecuteReader();
            }
            catch (PostgresException)
            {
                throw new Exception("Unable to create new subscription");
            }
            dbconnection.Close();
        }

        public override void Delete(long id)
        {
            NpgsqlConnection connection = dbconnection.Open();
            NpgsqlCommand command = connection.CreateCommand();
            command.CommandText = "DELETE FROM public.subscriptions WHERE id = :id";
            command.Parameters.Add(new NpgsqlParameter("id", (int)id));
            command.ExecuteNonQuery();
            dbconnection.Close();
        }

        public override Subscription Get(long id)
        {
            NpgsqlConnection connection = dbconnection.Open();
            NpgsqlCommand command = connection.CreateCommand();
            command.CommandText = "SELECT * FROM public.subscriptions s WHERE s.id = :subscriptions_id";
            command.Parameters.Add(new NpgsqlParameter("subscriptions_id", id));
            NpgsqlDataReader reader = command.ExecuteReader();
            Subscription s = null;
            while (reader.Read())
            {
                s = new Subscription(Convert.ToInt32(reader.GetValue(0)), Convert.ToInt32(reader.GetValue(1)),
                                  Convert.ToDateTime(reader.GetValue(2)));
            }
            dbconnection.Close();
            return s;
        }

        public override List<Subscription> Get(int page)
        {
            List<Subscription> s_list = new List<Subscription>();
            NpgsqlConnection connection = dbconnection.Open();
            NpgsqlCommand command = connection.CreateCommand();
            command.CommandText = "SELECT * FROM public.subscriptions" +
                " LIMIT 10 OFFSET :offset";
            command.Parameters.Add(new NpgsqlParameter("offset", (page-1) * 10));
            NpgsqlDataReader reader = command.ExecuteReader();
            while (reader.Read())
            {
                Subscription s = new Subscription(Convert.ToInt32(reader.GetValue(0)), Convert.ToInt32(reader.GetValue(1)),
                                  Convert.ToDateTime(reader.GetValue(2)));
                s_list.Add(s);
            }
            dbconnection.Close();
            return s_list;
        }

        public override void Update(Subscription entity)
        {
            NpgsqlConnection connection = dbconnection.Open();
            NpgsqlCommand command = connection.CreateCommand();
            command.CommandText = "UPDATE public.subscriptions SET duration = :duration, date_when_get = :date_when_get WHERE id = :id";
            command.Parameters.Add(new NpgsqlParameter("id", entity.Id));
            command.Parameters.Add(new NpgsqlParameter("duration", entity.Duration));
            command.Parameters.Add(new NpgsqlParameter("date_when_get", entity.Date_when_get));
            try
            {
                NpgsqlDataReader reader = command.ExecuteReader();
            }
            catch (PostgresException)
            {
                throw new Exception("Unable to edit subscription");
            }
            dbconnection.Close();
        }
        public List<Subscription> GetRange(int fromDuration,int toDuration)
        {
            List<Subscription> s_list = new List<Subscription>();
            NpgsqlConnection connection = dbconnection.Open();
            NpgsqlCommand command = connection.CreateCommand();
            command.CommandText = "SELECT * FROM public.subscriptions WHERE duration > :from AND duration < :to";
            command.Parameters.Add(new NpgsqlParameter("from", fromDuration));
            command.Parameters.Add(new NpgsqlParameter("to",toDuration));

            NpgsqlDataReader reader = command.ExecuteReader();
            while (reader.Read())
            {
                Subscription s = new Subscription(Convert.ToInt32(reader.GetValue(0)), Convert.ToInt32(reader.GetValue(1)),
                                  Convert.ToDateTime(reader.GetValue(2)));
                s_list.Add(s);
            }
            dbconnection.Close();
            return s_list;
        }

    }
}
