﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace lab2_db.Model
{
    class Author
    {
        public int Id { get; set; }
        public String Name { get; set; }
        public String Bio { get; set; }

        public Author(int id, String name, String bio)
        {
            this.Id = id;
            this.Name = name;
            this.Bio = bio;
        }
    }
}
