﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace lab2_db.Model
{
    class Book
    {
        public int Id { get; set; }
        public String Name { get; set; }
        public String Text { get; set; }
        public int Reader_id { get; set; }
        public int Author_id { get; set; }
        public Reader Reader { get; set; }
        public Author Author { get; set; }
        public Book(int id, String name,String text)
        {
            this.Id = id;
            this.Name = name;
            this.Text = text;
        }
        public Book(int id, String name, String text,int author_id)
        {
            this.Id = id;
            this.Name = name;
            this.Text = text;
            this.Author_id = author_id;
        }
        public Book(int id, String name, String text, int author_id,int reader_id)
        {
            this.Id = id;
            this.Name = name;
            this.Text = text;
            this.Author_id = author_id;
            this.Reader_id = reader_id;
        }
        public Book(int id, String name, String text, int reader_id, int author_id, String aName,String aBio,String rName,String rBio, int subscription_id, int duration, DateTime date)
        {
            this.Id = id;
            this.Name = name;
            this.Text = text;
            this.Author_id = author_id;
            this.Reader_id = reader_id;
            Reader = new Reader(reader_id, rName, rBio,subscription_id,duration,date);
            Author = new Author(author_id,aName,aBio);
        }
    }
}
