﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace lab2_db.Model
{
    class Reader
    {
        public int Id { get; set; }
        public String Name { get; set; }
        public String Bio { get; set; }
        public int Subscription_id { get; set; }
        public Subscription Subscription { get; set; }

        public Reader(int id, String name, String bio)
        {
            this.Id = id;
            this.Name = name;
            this.Bio = bio;
        }
        public Reader(int id, String name, String bio,int subscription_id)
        {
            this.Id = id;
            this.Name = name;
            this.Bio = bio;
            this.Subscription_id = subscription_id;
        }
        public Reader(int id, String name, String bio, int subscription_id, int duration, DateTime date)
        {
            this.Id = id;
            this.Name = name;
            this.Bio = bio;
            this.Subscription_id = subscription_id;
            Subscription = new Subscription(subscription_id,duration,date);
        }
    }
}
