﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace lab2_db.Model
{
    class Subscription
    {
        public int Id { get; set; }
        public int Duration { get; set; }
        public DateTime Date_when_get { get; set; }
        public Subscription(int id,int duration)
        {
            this.Id = id;
            this.Duration = duration;
            Date_when_get = DateTime.Now;
        }
        public Subscription(int id, int duration,DateTime date)
        {
            this.Id = id;
            this.Duration = duration;
            Date_when_get = date;
        }

    }
}
