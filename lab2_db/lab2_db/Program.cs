﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ConsoleTableExt;
using lab2_db.Controller;
using lab2_db.Database;
using lab2_db.Model;

namespace lab2_db
{
    class Program
    {
        static void Main(string[] args)
        {
            DBConnection dB = new DBConnection();
            MyController controller = new MyController(dB);
            controller.Start();
        }
    }
}
