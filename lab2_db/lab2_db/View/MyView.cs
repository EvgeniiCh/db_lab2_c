﻿using lab2_db.Database;
using lab2_db.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace lab2_db.View
{
    class MyView
    {
        public MyView() { }
        public void printMenu()
        {
            Console.Clear();
            Console.WriteLine("1.Authors");
            Console.WriteLine("2.Books");
            Console.WriteLine("3.Readers");
            Console.WriteLine("4.Subscriptions");
            Console.WriteLine("5.Get by full phase in authors");
            Console.WriteLine("6.Get all with not included word in authors");
            Console.WriteLine("q.exit");

        }
        public void printMess(String mess)
        {
            Console.WriteLine(mess);
        }
        public void clearAndPrintMess(String mess)
        {
            clearScreen();
            Console.WriteLine(mess);
        }
        public void printWrongInput()
        {
            Console.WriteLine("Wrong input!Try again!");
        }
        public char getKey()
        {
            return Console.ReadKey().KeyChar;
        }
        public String getString()
        {
            return Console.ReadLine();
        }
        public void wait()
        {
            Console.Write("Press any key to get back: ");
            Console.ReadKey();
        }
        public void clearScreen()
        {
            Console.Clear();
        }
        public void printStars()
        {
            printMess("*****************************************");
        }
        public void waitForUnpressKey()
        {
            while (Console.KeyAvailable == false)
                Thread.Sleep(250);
        }

        public void printSubMenu(String name)
        {
            Console.Clear();
            Console.WriteLine(name);
            Console.WriteLine("1.Add");
            Console.WriteLine("2.Get by id");
            Console.WriteLine("3.Get all from page");
            Console.WriteLine("4.Delete");
            Console.WriteLine("5.Update");
            Console.WriteLine("b.back");
        }
        public void printSubscriptionMenu()
        {
            Console.Clear();
            Console.WriteLine("Subscription");
            Console.WriteLine("1.Add");
            Console.WriteLine("2.Get by id");
            Console.WriteLine("3.Get all from page");
            Console.WriteLine("4.Delete");
            Console.WriteLine("5.Update");
            Console.WriteLine("6.Get range");
            Console.WriteLine("b.back");
        }
        public void subscriptionToString(Subscription sub)
        {
            printMess("Index: " + sub.Id);
            printMess("Duration of subscription: " + sub.Duration);
            printMess("Date when subscription: " + sub.Date_when_get);
        }

        public void authorToString(Author author)
        {
            printMess("Index: " + author.Id);
            printMess("Author name: " + author.Name);
            printMess("Author bio: " + author.Bio);
        }
        public void readerToString(Reader reader)
        {
            printMess("Index: " + reader.Id);
            printMess("Reader name: " + reader.Name);
            printMess("Reader bio: " + reader.Bio);
            printMess("Reader subscription: ");
            printMess("--");
            subscriptionToString(reader.Subscription);
            printMess("--");
        }
        public void bookToString(Book book)
        {
            printMess("Index: " + book.Id);
            printMess("Book name: " + book.Name);
            printMess("Book text: " + book.Text);
            printMess("Book author: ");
            printMess("//");
            authorToString(book.Author);
            printMess("//");
            printMess("Book reader: ");
            printMess("//");
            readerToString(book.Reader);
            printMess("//");

        }

        public void subscriptionListToString(List<Subscription> list,int page)
        {
            if(page!=0)
                printMess("Subscriptions page "+page);
            else
                printMess("Subscriptions");
            foreach (Subscription sub in list)
            {
                subscriptionToString(sub);
                printStars();
            }
        }
        public void authorListToString(List<Author> list, int page)
        {
            if (page != 0)
                printMess("Authors page " + page);
            else
                printMess("Authors");
            foreach (Author author in list)
            {
                authorToString(author);
                printStars();
            }
        }
        public void readersListToString(List<Reader> list, int page)
        {
            if (page != 0)
                printMess("Reader page " + page);
            else
                printMess("Reader");
            foreach (Reader reader in list)
            {
                readerToString(reader);
                printStars();
            }
        }
        public void booksListToString(List<Book> list, int page)
        {
            if (page != 0)
                printMess("Book page " + page);
            else
                printMess("Book");
            foreach (Book book in list)
            {
                bookToString(book);
                printStars();
            }
        }
        public void serchResListToString(List<SearchRes> list)
        {
            
            printMess("Serch result");
            foreach (SearchRes sr in list)
            {
                printMess("Index: " + sr.Id);
                printMess("Attribute: " + sr.Attr);
                printMess("Hedline: " + sr.Ts_headline);
                printStars();
            }
        }
    }
}
